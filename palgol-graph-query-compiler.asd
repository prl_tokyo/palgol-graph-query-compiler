;;;; palgol-graph-query-compiler.asd

(asdf:defsystem #:palgol-graph-query-compiler
  :description "Palgol Graph Query Compiler"
  :author "Smith Dhumbumroong <zodmaner@gmail.com>"
  :license "MIT"
  :depends-on (#:alexandria
               #:arrow-macros
               #:trivia
               #:cl-ppcre
               #:c-mera
               #:cm-c++
               #:cmu-c++)
  :serial t
  :components ((:file "package")
               (:file "parser")
               (:file "transformer")
               (:file "code-gen")
               (:file "c++-processor")
               (:file "dfp")               
               (:file "palgol-graph-query-compiler")))

