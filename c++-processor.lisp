;;;; c++-processor.lisp

(in-package #:palgol-graph-query-compiler)

;;; Generates C++ code from a file containing sexpr-C++ code

(declaim (ftype (function (pathname pathname) *) compile-sexpr-c++-file-to-c++-file))
(defun compile-sexpr-c++-file-to-c++-file (src dst)
  "Read a SRC file containing sexpr-C++ code and compile them into C++
code and write the result out to a DST file."
  ;; needs to switch the current package to CMU-C++, else the
  ;; processor won't work
  (in-package #:cmu-c++)
  (let ((*print-case* :downcase))
    (cm-c++::cxx-processor (list "dummy" "-o" (namestring dst) (namestring src))))
  ;; switches back to the compiler package because CMU-C++ shadowed too
  ;; many standard symbols
  (in-package #:palgol-graph-query-compiler))
