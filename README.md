# Palgol Graph Query Compiler (PGQC) #

This is Palgol Graph Query Compiler, or **PGQC**.

PGQC compiles a query file into a C++ header used by Palgol
Vertex-Centric Graph Pattern Matching Framework.

A query is described using a custom declarative high-level query
language called Palgol Graph Query Langauge (**PGQL**).  The syntax of
PGQL is inspired by [Cypher](https://en.wikipedia.org/wiki/Cypher_Query_Language) and [GraphQL](http://graphql.org/).

## Palgol Graph Query Language (PGQL) ##

This section describes the current syntax of Palgol Graph Query
Language in EBNF notations:

    <query> ::= <clause> "{" { <pattern> } "}";
    <clause> ::= "match";
    <pattern> ::= "{" <vertex_label> <relation> <vertex_label> "}";
    <relation> ::= "->"
                 | "-[" <edge_label> "]->";
    <vertex_label> ::= <integer>;
    <edge_label> ::= <integer>;
    <integer> ::= { <decimal_digit> };
    <decimal_digit> ::= "0" | "1" | "2" | "3" | "4"
                      | "5" | "6" | "7" | "8" | "9";

For examples, the following query graph:

    +---+    +---+    +---+--->+---+
    | 1 |--->| 2 |<---| 3 |    | 4 |
    +---+    +---+    +---+<---+---+

can be described using PGQL as follows.

    match {
      { 1 -> 2 }
      { 3 -> 2 }
      { 3 -> 4 }
      { 4 -> 3 }
    }

The same query graph but now with edge labels:

    +---+     +---+     +---+--6->+---+
    | 1 |--5->| 2 |<-6--| 3 |     | 4 |
    +---+     +---+     +---+<-7--+---+

can be described as follows.

    match {
      { 1 -[5]-> 2 }
      { 3 -[6]-> 2 }
      { 3 -[6]-> 4 }
      { 4 -[7]-> 3 }
    }

## Getting Started ##

Given a query file, `query-01.palql`, that contains the following
query.

    match {
      { 1 -> 2 }
      { 3 -> 2 }
      { 3 -> 4 }
      { 4 -> 3 }
    }

The following command will compile the above query file into a C++
header file named `query-01.hpp`:

    $ ./pgqc query-01.palql

To use the generated `query-01.hpp` header file that contains the
query, just include it in Palgol's `external.hpp` (or `auxiliary.hpp`)
header file.

## Building ##

First, install the following dependencies.

* [SBCL](http://www.sbcl.org) (version 1.3.11 or later)
* [Quicklisp](https://www.quicklisp.org)

Next, clone this repository and issue a `make` command.

    $ git clone https://bitbucket.org/prl_tokyo/palgol-graph-query-compiler.git
    $ cd /palgol-graph-query-compiler/
    $ make

If all goes well, a binary named `pgqc` should be created at the end
of the compilation process.

## Dependencies ##

* Alexandria
* Arrow-macros
* Trivia
* CL-PPCRE
* C-Mera

## Author ##

* Smith Dhumbumroong (<zodmaner@gmail.com>)
