# A simple makefile template for creating a standalone executable Common Lisp image.
#
# Dependencies:
#  * SBCL
#  * Quicklisp
#
# Author: Smith Dhumbumroong <zodmaner@gmail.com>

SYSNAME = palgol-graph-query-compiler
TARGET = pgqc
EFUNCTION = $(SYSNAME):main

all: $(TARGET)

$(TARGET): buildapp quicklisp-manifest.txt
	./buildapp --manifest-file quicklisp-manifest.txt \
			   --compress-core \
			   --load-system $(SYSNAME) \
               --asdf-tree "./" \
			   --entry $(EFUNCTION) \
			   --output $(TARGET)

quicklisp-manifest.txt: c-mera-master
	sbcl --no-userinit --no-sysinit --non-interactive \
		 --load ~/quicklisp/setup.lisp \
		 --eval '(load "./c-mera-master/c-mera.asd")' \
         --eval '(load "$(SYSNAME).asd")' \
		 --eval '(ql:quickload "$(SYSNAME)")' \
		 --eval '(ql:write-asdf-manifest-file "quicklisp-manifest.txt")'

c-mera-master:
	wget https://github.com/kiselgra/c-mera/archive/master.zip
	unzip master.zip

buildapp:
	sbcl --no-userinit --no-sysinit --non-interactive \
		 --load ~/quicklisp/setup.lisp \
		 --eval '(ql:quickload "buildapp")' \
		 --eval '(buildapp:build-buildapp)'

clean:
	-rm quicklisp-manifest.txt
