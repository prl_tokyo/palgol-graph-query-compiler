;;;; dfp.lisp

(in-package #:palgol-graph-query-compiler)

;;; Generates sexpr-C++ from a depth-first-path query.

(defun raw-string-to-raw-dfp-query (raw-string)
  "Parse the string containing vertex labels \(representing a
depth-first-path query\) into an s-expression."
  (with-input-from-string (s raw-string)
    (loop :for o := (read s nil nil) :while o :collect o)))

(defun raw-dfp-query-to-dfp-query (raw-dfp-query)
  "Transform an s-expression containing vertex labels \(representing a
depth-first-path query\) into a valid s-expression representation of
the query."
  (loop :with query-len := (length raw-dfp-query)
     :for i :from 0 :upto query-len :by 2
     :for j :from 2 :upto query-len :by 2
     :collect (subseq raw-dfp-query i j)))

(defun compile-dfp-to-sexpr-c++ (src dst)
  "Compile a file containing vertex labels \(representing a
depth-first-path query\) into a sexpr-C++ code that describe the query
and write it out to a file."
  (with-open-file (output-file dst
                               :direction :output
                               :if-exists :supersede
                               :if-does-not-exist :create)
    (let ((*standard-output* output-file)
          (*print-case* :downcase))
      (-> src
        (query-file-to-raw-string)
        (raw-string-to-raw-dfp-query)
        (raw-dfp-query-to-dfp-query)
        (query-to-ir)
        (ir-to-sexpr-c++)
        (write)))))
