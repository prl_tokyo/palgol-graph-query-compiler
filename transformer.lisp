;;;; transformer.lisp

(in-package #:palgol-graph-query-compiler)

;;; Transforms a given query into an intermediate representation.

(defconstant +empty-set+ '()
  "Using lists as sets.")

(defconstant +wildcard+ -7102
  "A wildcard value that will match against everything.")

(declaim (ftype (function (list) hash-table) query-to-ir))
(defun query-to-ir (query)
  "Transform a valid query into an intermediate representation."
  (loop
     :with ir := (make-hash-table :test 'equal)
     :for p :in query :do
     (trivia:match p
       ((list* a b e)
        (let* ((a-adjs (gethash a ir (make-hash-table :test 'equal)))
               (b-adjs (gethash b ir (make-hash-table :test 'equal)))
               (children (gethash :children a-adjs +empty-set+))
               (parents (gethash :parents b-adjs +empty-set+))
               (a-pair (if e (cons (car e) b) (cons +wildcard+ b)))
               (b-pair (if e (cons (car e) a) (cons +wildcard+ a))))
          (setf (gethash :children a-adjs) (adjoin a-pair children))
          (setf (gethash :parents b-adjs) (adjoin b-pair parents))
          (setf (gethash a ir) a-adjs)
          (setf (gethash b ir) b-adjs))))
     :finally (return ir)))
