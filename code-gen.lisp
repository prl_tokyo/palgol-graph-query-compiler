;;;; code-gen.lisp

(in-package #:palgol-graph-query-compiler)

;;; Generates C-Mera sexpr-C++ code that describe the query from the
;;; intermediate data structures.

(defvar *graph-sim* nil)

(declaim (ftype (function nil list) make-preamble))
(defun make-preamble ()
  "Create the necessary preamble."
  `(progn
     (include '<vector>)
     (include '<algorithm>)
     (using-namespace std)))

(declaim (ftype (function (hash-table) list)
                make-match-vertex-label-function
                make-match-adjacent-vertices-function
                ir-to-sexpr-c++))

(defun make-match-vertex-label-function (ir)
  "Create a match_vertex_label function from a given intermediate
representation."
  `(template ((class T))
             (function match_vertex_label ((T v_label)) -> bool
                       ,@(loop :for v-l :being :the :hash-keys :of ir :collect
                            `(if (== v_label ,v-l)
                                 (return true)))
                       (return false))))

(defun make-match-adjacent-vertices-function (ir)
  "Create a match_adjacent_vertices function from a given intermediate
representation."
  (flet ((gen-vector-of-pairs-init-code (vector lst)
           (declare (symbol vector) (list lst))
           `(progn
              (funcall (oref ,vector clear))
              ,@(loop :for pair :in lst :collect
                   `(funcall (oref ,vector push_back)
                             (funcall make_pair ,(car pair) ,(cdr pair))))))
         (gen-vector-of-labels-init-code (vector-of-pairs vector-of-labels)
           (declare (symbol vector-of-pairs vector-of-labels))
           (alexandria:with-gensyms (i)
             `(progn
                (funcall (oref ,vector-of-labels clear))
                (for-each (,i ,vector-of-pairs)
                          (funcall (oref ,vector-of-labels push_back) (oref ,i second))))))
         (gen-vector-of-labels-init-code/query (vector-of-pairs vector-of-labels vector-of-pairs-2)
           (declare (symbol vector-of-pairs vector-of-labels vector-of-pairs-2))
           (alexandria:with-gensyms (i)
             `(progn
                (funcall (oref ,vector-of-labels clear))
                (for-each (,i ,vector-of-pairs)
                          (if (== (oref ,i first) ,+wildcard+)
                              (funcall (oref ,vector-of-labels push_back) (oref ,i second))
                              (progn
                                (if (!= (funcall find
                                                 (funcall (oref ,vector-of-pairs-2 begin))
                                                 (funcall (oref ,vector-of-pairs-2 end))
                                                 ,i)
                                        (funcall (oref ,vector-of-pairs-2 end)))
                                    (funcall (oref ,vector-of-labels push_back) (oref ,i second))
                                    (funcall (oref ,vector-of-labels push_back)
                                             (* (oref ,i second) -1)))))))))
         (gen-sort-vector-code (vector)
           (declare (symbol vector))
           `(funcall sort (funcall (oref ,vector begin)) (funcall (oref ,vector end))))
         (gen-vector-includes-code (v1 v2)
           (declare (symbol v1 v2))
           `(funcall includes
                     (funcall (oref ,v1 begin)) (funcall (oref ,v1 end))
                     (funcall (oref ,v2 begin)) (funcall (oref ,v2 end)))))
    `(template
      ((class T))
      (function match_adjacent_vertices
                ((T v_label)
                 ((instantiate vector ((instantiate pair (T) (T)))) v_children)
                 ((instantiate vector ((instantiate pair (T) (T)))) v_parents)) -> bool
                ,@(loop
                     :for v-l :being :the :hash-keys :of ir :using (hash-value adjs) :collect
                     (let ((children (gethash :children adjs))
                           (parents (gethash :parents adjs)))
                       (alexandria:with-gensyms (v_chls v_pals q_chs q_pas q_chls q_pals)
                         `(if (== v_label ,v-l)
                              (decl (,@(if children
                                           `(((instantiate vector
                                                           ((instantiate pair (T) (T)))) ,q_chs)
                                             ((instantiate vector (T)) ,q_chls)
                                             ((instantiate vector (T)) ,v_chls)))
                                     ,@(if (and parents (not *graph-sim*))
                                           `(((instantiate vector
                                                           ((instantiate pair (T) (T)))) ,q_pas)
                                             ((instantiate vector (T)) ,q_pals)
                                             ((instantiate vector (T)) ,v_pals))))
                                    ,(if children
                                         `(progn
                                            ,(gen-vector-of-pairs-init-code q_chs children)
                                            ,(gen-vector-of-labels-init-code/query
                                              q_chs q_chls 'v_children)
                                            ,(gen-vector-of-labels-init-code 'v_children v_chls)
                                            ,(gen-sort-vector-code q_chls)
                                            ,(gen-sort-vector-code v_chls)))
                                    ,(if (and parents (not *graph-sim*))
                                         `(progn
                                            ,(gen-vector-of-pairs-init-code q_pas parents)
                                            ,(gen-vector-of-labels-init-code/query
                                              q_pas q_pals 'v_parents)
                                            ,(gen-vector-of-labels-init-code 'v_parents v_pals)
                                            ,(gen-sort-vector-code q_pals)
                                            ,(gen-sort-vector-code v_pals)))
                                    ,(cond
                                      ((and children parents (not *graph-sim*))
                                       `(if (and ,(gen-vector-includes-code v_chls q_chls)
                                                 ,(gen-vector-includes-code v_pals q_pals))
                                            (return true)))
                                      (children
                                       `(if ,(gen-vector-includes-code v_chls q_chls)
                                            (return true)))
                                      ((and parents (not *graph-sim*))
                                       `(if ,(gen-vector-includes-code v_pals q_pals)
                                            (return true)))
                                      ((and *graph-sim* (not children))
                                       '(return true))))))))
                (return false)))))

(defun ir-to-sexpr-c++ (ir)
  "Generate C-Mera sexpr-C++ code that describe the query from a given
intermediate representation."
  `(progn
     ,(make-preamble)
     ,(make-match-vertex-label-function ir)
     ,(make-match-adjacent-vertices-function ir)))
