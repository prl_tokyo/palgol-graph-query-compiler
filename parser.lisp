;;;; parser.lisp

(in-package #:palgol-graph-query-compiler)

;;; The parser

(declaim (ftype (function (pathname) simple-string) query-file-to-raw-string))
(defun query-file-to-raw-string (query-file)
  "Read and return the content of a file containing the query as a
string."
  (alexandria:read-file-into-string query-file))

(declaim (ftype (function (simple-string) list) raw-string-to-raw-query))
(defun raw-string-to-raw-query (raw-string)
  "Parse and transform the raw string containing the query into an
s-expression and return it."
  (-<>> raw-string
    (substitute #\( #\{)
    (substitute #\) #\})
    (with-input-from-string (is <>)
      (loop :for obj := (read is nil nil) :while obj :collect obj))))

(declaim (ftype (function (list) list) raw-query-to-query))
(defun raw-query-to-query (raw-query)
  "Parse the raw query into a valid s-expression representation of the
query and \(sub\) patterns inside and return it."
  (trivia:match raw-query
    ((list 'match raw-patterns)
     (loop :for r-p :in raw-patterns :collect
        (trivia:match r-p
          ((list a '-> b) (list a b))
          ((list a c b)
           (list a b (-<> c
                       (string)
                       (nth-value 1 (cl-ppcre:scan-to-strings "-\\[ *([0-9]+) *\\]->" <>))
                       (aref <> 0)
                       (parse-integer)))))))))

(declaim (ftype (function (pathname) list) query-file-to-query))
(defun query-file-to-query (query-file)
  "Read, transform, and parse the content of a file containing the
query into a valid s-expression representation of the query and
\(sub\) patterns inside and return it."
  (-> query-file
    (query-file-to-raw-string)
    (raw-string-to-raw-query)
    (raw-query-to-query)))
