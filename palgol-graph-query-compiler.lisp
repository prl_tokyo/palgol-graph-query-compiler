;;;; palgol-graph-query-compiler.lisp

(in-package #:palgol-graph-query-compiler)

;;; "palgol-pattern-compiler" goes here. Hacks and glory await!

(declaim (ftype (function (list) *) main))
(defun main (args)
  "The main function used as an entry point when running as a compiled
binary."
  (in-package #:palgol-graph-query-compiler)
  (flet ((compile-src-to-dst (src dst)
           (declare (pathname src dst))
           (with-open-file (output-file dst
                                        :direction :output
                                        :if-exists :supersede
                                        :if-does-not-exist :create)
             (let ((*standard-output* output-file)
                   (*print-case* :downcase)
                   (src-type (pathname-type src)))
               (if (string= src-type "palql")
                   (-> src
                     (query-file-to-query)
                     (query-to-ir)
                     (ir-to-sexpr-c++)
                     (write))
                   (format t "Error: Wrong file type (expect a file with \".palql\" extension).~%"))))))
    (trivia:match args
      ((list _ "-d" src)
       (let* ((src-path (pathname src))
              (src-dir (pathname-directory src-path))
              (src-name (pathname-name src-path))
              (ir-path (make-pathname :directory src-dir :name src-name :type "lisp"))
              (dst-path (make-pathname :directory src-dir :name src-name :type "hpp")))
         (compile-dfp-to-sexpr-c++ src-path ir-path)
         (compile-sexpr-c++-file-to-c++-file ir-path dst-path)))
      ((list _ "--graph-sim" src)
       (let* ((src-path (pathname src))
              (src-dir (pathname-directory src-path))
              (src-name (pathname-name src-path))
              (ir-path (make-pathname :directory src-dir :name src-name :type "lisp"))
              (dst-path (make-pathname :directory src-dir :name src-name :type "hpp"))
              (*graph-sim* t))
         (compile-src-to-dst src-path ir-path)
         (compile-sexpr-c++-file-to-c++-file ir-path dst-path)))
      ((list _ src dst)
       (let* ((src-path (pathname src))
              (src-dir (pathname-directory src-path))
              (src-name (pathname-name src-path))
              (ir-path (make-pathname :directory src-dir :name src-name :type "lisp"))
              (dst-path (pathname dst)))
         (compile-src-to-dst src-path ir-path)
         (compile-sexpr-c++-file-to-c++-file ir-path dst-path)))
      ((list _ src)
       (let* ((src-path (pathname src))
              (src-dir (pathname-directory src-path))
              (src-name (pathname-name src-path))
              (ir-path (make-pathname :directory src-dir :name src-name :type "lisp"))
              (dst-path (make-pathname :directory src-dir :name src-name :type "hpp")))
         (compile-src-to-dst src-path ir-path)
         (compile-sexpr-c++-file-to-c++-file ir-path dst-path)))
      ((list binary-name)
       (format t "Usage: ~A [OPTIONS] SOURCE [DEST]~%" binary-name)))))
